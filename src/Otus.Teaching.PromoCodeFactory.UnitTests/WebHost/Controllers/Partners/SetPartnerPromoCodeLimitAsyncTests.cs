﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }


        [Fact]
        public async void Test_PartnerNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            var partnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 10,
                EndDate = DateTime.Now.AddDays(20)
            };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void Test_PartnerIsNotActive()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.IsActive = false;
            var partnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 10,
                EndDate = DateTime.Now.AddDays(20)
            };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void Test_LimitToZero()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.PartnerLimits.FirstOrDefault().CancelDate = null;


            var partnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 10,
                EndDate = DateTime.Now.AddDays(20)
            };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = (IActionResult)await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            // Assert
            Assert.Equal(1,partner.PartnerLimits.Where(x =>
                !x.CancelDate.HasValue&& partner.NumberIssuedPromoCodes == 0).ToList().Count);
            
        }

        [Fact]
        public async void Test_LimitToNonActive()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.PartnerLimits.FirstOrDefault().CancelDate = null;
            

            var partnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 10,
                EndDate = DateTime.Now.AddDays(20)
            };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            // Assert
            Assert.Equal(1, partner.PartnerLimits.Where(x =>
                    x.CancelDate.HasValue && x.CancelDate.Value.Date.ToShortDateString()== DateTime.Now.Date.ToShortDateString()).ToList().Count);
        }

        [Fact]
        public async void Test_LimitIsMoreThenZero()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.PartnerLimits.FirstOrDefault().CancelDate = new DateTime(2022, 10, 9);
            var partnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 0,
                EndDate = DateTime.Now.AddDays(20)
            };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void Test_LimitSaved()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.PartnerLimits.FirstOrDefault().CancelDate = new DateTime(2022, 10, 9);
            var partnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 10,
                EndDate = DateTime.Now.AddDays(20)
            };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
    }
}